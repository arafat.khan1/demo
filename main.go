package main

import (
	"demo/config"
	"demo/router"
	"demo/server"
	"log"
)

func main() {

	srv := server.New()
	router.Routes(srv)
	err := config.InitEnvironmentVariables()
	if err != nil {
		log.Fatal("envVars error: " + err.Error())
	}
	srv.Logger.Fatal(srv.Start(":" + config.ServerPort))
}
