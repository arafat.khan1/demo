package config

import (
	"github.com/joho/godotenv"
	"log"
	"os"
)

// ServerPort and RedisPort are embedded into actual code, as they are part of the codebase
//If you run Redis in a different port, please change the default value
//If you want to use a different port for the application, change the server port. Please also make changes to docker file (expose the same port)
const ServerPort = "4040"

//envVars

//temp variable
var RunMode string

func InitEnvironmentVariables() error {

	//checking runMode
	RunMode = os.Getenv("RUN_MODE")
	if RunMode == "" {
		RunMode = DEVELOP
	}
	var err error
	log.Println("RUN MODE:", RunMode)

	//loading envArs from .env file is runMode != PRODUCTION
	if RunMode != PRODUCTION {
		err = godotenv.Load()
		if err != nil {
			log.Println("ERROR: ", err.Error())
		}
	}
	return nil
	//DB CREDENTIALS + Cluster Endpoint + Others
}
