FROM golang:1.17 as builder

WORKDIR /app

COPY go.mod go.sum ./

RUN go mod download

COPY . .

RUN go build -o /app/bin/go .

FROM ubuntu

RUN useradd -ms /bin/bash pathao-dev

WORKDIR /home/pathao-dev

COPY --from=builder /app/bin .


EXPOSE 4040

USER pathao-dev
CMD ["./go"]