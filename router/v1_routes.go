package router

import (
	v1 "demo/api/v1"
	"github.com/labstack/echo/v4"
)

func V1Routes(group *echo.Group) {
	group.POST("/string", v1.CacheController().Create)
	group.GET("/string", v1.CacheController().Get)
}
