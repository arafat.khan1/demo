package v1

import (
	"demo/dto"
	"demo/utils"
	"github.com/labstack/echo/v4"
	"log"
	"net/http"
)

var storedString string

type CacheControllerInf interface {
	Create(c echo.Context) error
	Get(c echo.Context) error
}

type CacheControllerInstance struct {
}

func CacheController() CacheControllerInf {
	return new(CacheControllerInstance)
}

func (e CacheControllerInstance) Create(c echo.Context) error {
	var input dto.InputObject
	if err := c.Bind(&input); err != nil {
		log.Println("input error: ", err.Error())
		return c.JSON(http.StatusBadRequest, err.Error())
	}
	utils.TrimSpaces(&input.Value)
	utils.TrimSpaces(&input.Value)
	if !utils.DtoValidation(&input) {
		log.Println("json invalid")
		return c.JSON(http.StatusBadRequest, "key and/or value is null")
	}
	storedString = input.Value
	return c.JSON(http.StatusOK, "successfully added")
}

func (e CacheControllerInstance) Get(c echo.Context) error {
	respObj := dto.InputObject{Value: storedString}
	return c.JSON(http.StatusOK, respObj)
}
